package com.ua.epam.project.repos;

import com.ua.epam.project.entities.Periodical;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface PeriodicalRepo extends CrudRepository<Periodical, Long>, JpaSpecificationExecutor<Periodical> {
    Page<Periodical> findAll(Specification specification, Pageable pageable);
    Page<Periodical> findAll(Pageable pageable);
    Page<Periodical> findByFindName(String name, Pageable pageable);

}
