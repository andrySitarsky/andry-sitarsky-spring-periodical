package com.ua.epam.project.repos;

import com.ua.epam.project.entities.UserInfo;
import org.springframework.data.repository.CrudRepository;

public interface UserInfoRepo extends CrudRepository<UserInfo, Long> {
}
