package com.ua.epam.project.repos;

import com.ua.epam.project.entities.Price;
import org.springframework.data.repository.CrudRepository;

public interface PriceRepo extends CrudRepository<Price, Long> {

}
