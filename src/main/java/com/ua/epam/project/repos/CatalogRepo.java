package com.ua.epam.project.repos;

import com.ua.epam.project.entities.Catalog;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CatalogRepo extends CrudRepository<Catalog, Long> {
    boolean existsByNameUa(String name);
    boolean existsByNameEn(String name);


}
