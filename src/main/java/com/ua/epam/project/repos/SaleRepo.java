package com.ua.epam.project.repos;

import com.ua.epam.project.entities.Sale;
import org.springframework.data.repository.CrudRepository;

public interface SaleRepo extends CrudRepository<Sale, Long> {
}
