package com.ua.epam.project.entities;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "periodicals")
@Getter
@Setter
@NoArgsConstructor
public class Periodical implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String findName;
    private String pathImage;
    private String info;

    @OneToMany(mappedBy = "periodical", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Price> prices;

    @OneToMany(mappedBy = "periodical", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Sale> sales;

    @ManyToMany
    @JoinTable(
            name = "catalog_periodical",
            joinColumns = { @JoinColumn(name = "periodical_id") },
            inverseJoinColumns = { @JoinColumn(name = "catalog_id") }
    )
    private Set<Catalog> catalogs = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "user_periodical",
            joinColumns = { @JoinColumn(name = "periodical_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private Set<User> users;

}
