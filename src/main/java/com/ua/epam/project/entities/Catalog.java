package com.ua.epam.project.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="catalogs")
public class Catalog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nameUa;
    private String nameEn;

    @ManyToMany
    @JoinTable(
            name = "parent_catalog",
            joinColumns = { @JoinColumn(name = "catalog_id") },
            inverseJoinColumns = { @JoinColumn(name = "parent_id") }
    )
    private List<Catalog> parent;

    @ManyToMany
    @JoinTable(
            name = "parent_catalog",
            joinColumns = { @JoinColumn(name = "parent_id") },
            inverseJoinColumns = { @JoinColumn(name = "catalog_id") }
    )
    private Set<Catalog> children;

    @ManyToMany
    @JoinTable(
            name = "catalog_periodical",
            joinColumns = { @JoinColumn(name = "catalog_id") },
            inverseJoinColumns = { @JoinColumn(name = "periodical_id") }
    )
    private Set<Periodical> periodicals = new HashSet<>();

}
