package com.ua.epam.project.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "price")
@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class Price implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodical_id")
    private Periodical periodical;

    @NonNull
    private LocalDate startData;
    @NonNull
    private LocalDate endData;
    @NonNull
    private BigDecimal price;
}
