package com.ua.epam.project.config;

import com.ua.epam.project.config.dialects.PeriodicalDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

import java.util.HashSet;
import java.util.List;

public class DialectConfig {
    @Bean
    public ViewResolver viewResolver(List<AbstractDialect> dialects, SpringTemplateEngine templateEngine) {
        templateEngine.setDialects(new HashSet<>(dialects));

        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine);
        resolver.setCharacterEncoding("UTF-8");
        return resolver;
    }

    @Bean
    public PeriodicalDialect periodicalDialect(){return new PeriodicalDialect();}
}
