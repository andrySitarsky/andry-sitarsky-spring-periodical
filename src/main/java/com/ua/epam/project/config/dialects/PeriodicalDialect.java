package com.ua.epam.project.config.dialects;

import com.ua.epam.project.config.dialects.processors.AbstractMenuProcessor;
import com.ua.epam.project.config.dialects.processors.AccordionMenuProcessor;
import com.ua.epam.project.config.dialects.processors.ItemMenuProcessor;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import java.util.HashSet;
import java.util.Set;

public class PeriodicalDialect extends AbstractProcessorDialect {
    private static final String HTML_PREFIX = "pmenu";
    private static final String DIALECT_NAME = "Menu tags for periodical";

    public PeriodicalDialect() {
        super(DIALECT_NAME, HTML_PREFIX, StandardDialect.PROCESSOR_PRECEDENCE);
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        Set<IProcessor> processors = new HashSet<>();
        processors.add(new AccordionMenuProcessor(dialectPrefix));
        processors.add(new ItemMenuProcessor(dialectPrefix));
        return processors;
    }
}
