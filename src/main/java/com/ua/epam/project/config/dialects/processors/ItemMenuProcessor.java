package com.ua.epam.project.config.dialects.processors;

import com.ua.epam.project.tld.ItemMenuTld;
import com.ua.epam.project.tld.SettingMenu;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.processor.element.IElementTagStructureHandler;

public class ItemMenuProcessor extends AbstractMenuProcessor {
    private static final String ATTR_NAME = "item";
    private static final int PRECEDENCE = 0;
    private ItemMenuTld menuTld = new ItemMenuTld();


    public ItemMenuProcessor(String dialectPrefix) {
        super(dialectPrefix, ATTR_NAME, PRECEDENCE);
    }

    @Override
    protected void fillModel(ITemplateContext context, SettingMenu settingMenu,
                             IElementTagStructureHandler structureHandler) {
       structureHandler.setBody(menuTld.printItem(settingMenu), false);
    }
}
