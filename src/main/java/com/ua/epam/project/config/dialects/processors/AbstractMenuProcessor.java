package com.ua.epam.project.config.dialects.processors;

import com.ua.epam.project.entities.Catalog;
import com.ua.epam.project.repos.CatalogRepo;
import com.ua.epam.project.tld.SettingImpl;
import com.ua.epam.project.tld.SettingMenu;
import org.springframework.context.ApplicationContext;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.IThymeleafRequestContext;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMenuProcessor extends AbstractElementTagProcessor {
    private String attributeName;


    public AbstractMenuProcessor( String dialectPrefix, String attribute, int precedence) {
        super(TemplateMode.HTML, dialectPrefix, null,
                true, attribute, false, precedence);
        attributeName = attribute;
    }

    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag,
                             IElementTagStructureHandler structureHandler) {
        final IEngineConfiguration configuration = context.getConfiguration();

        final IStandardExpressionParser parser =
                StandardExpressions.getExpressionParser(configuration);

        final IStandardExpression expression = parser.parseExpression(context,
                tag.getAttributeValue(attributeName));

        final String idStr = (String) expression.execute(context);

        /*Create settings*/
        final ApplicationContext appCtx = SpringContextUtils.getApplicationContext(context);
        final IThymeleafRequestContext reqCtx = SpringContextUtils.getRequestContext(context);
        final CatalogRepo repository = appCtx.getBean(CatalogRepo.class);
        List<Catalog> catalogs = new ArrayList();
        repository.findAll().forEach(catalogs::add);
        Catalog catalog = repository.findById(Long.parseLong(idStr)).get();
        final String locale = reqCtx.getLocale().getLanguage();
        boolean edit = (boolean) reqCtx.getModel().get("edit");
        final String all =context.getMessage(AbstractMenuProcessor.class,
                "menu.all", new Object[] {},true);
        SettingImpl setting = SettingImpl.builder().list(catalogs)
                .catalog(catalog).locale(locale).edit(edit).all(all).build();

        fillModel(context,setting, structureHandler);
    }
    protected abstract void fillModel(ITemplateContext context, SettingMenu settingMenu,
                                      IElementTagStructureHandler structureHandler);

}
