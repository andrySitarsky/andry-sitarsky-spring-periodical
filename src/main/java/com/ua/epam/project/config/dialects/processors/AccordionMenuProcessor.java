package com.ua.epam.project.config.dialects.processors;

import com.ua.epam.project.tld.AccordionMenuTld;
import com.ua.epam.project.tld.SettingMenu;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.unbescape.html.HtmlEscape;

public class AccordionMenuProcessor extends AbstractMenuProcessor{
    private static final String ATTR_NAME = "accordion";
    private static final int PRECEDENCE = 1;
    private AccordionMenuTld menuTld = new AccordionMenuTld();

    public AccordionMenuProcessor(String dialectPrefix) {
        super(dialectPrefix, ATTR_NAME, PRECEDENCE);
    }

    @Override
    protected void fillModel(ITemplateContext context, SettingMenu setting,
                             IElementTagStructureHandler structureHandler) {
        final IModelFactory modelFactory = context.getModelFactory();
        final IModel model = modelFactory.createModel();
        model.add(modelFactory.createOpenElementTag("div", "class", "text-left font-weight-bold"));
        model.add(modelFactory.createText(HtmlEscape.escapeHtml5(menuTld.printMenu(setting))));
        model.add(modelFactory.createCloseElementTag("div"));
        structureHandler.replaceWith(model, false);
    }
}
