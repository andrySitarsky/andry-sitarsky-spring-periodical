package com.ua.epam.project.services;

import com.ua.epam.project.dto.CatalogDTO;
import com.ua.epam.project.dto.CatalogsDTO;
import com.ua.epam.project.dto.PeriodicalDTO;
import com.ua.epam.project.entities.Catalog;
import com.ua.epam.project.entities.Periodical;
import com.ua.epam.project.entities.Price;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.repos.CatalogRepo;
import com.ua.epam.project.repos.PeriodicalRepo;
import com.ua.epam.project.repos.PriceRepo;
import com.ua.epam.project.repos.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AdminServes {

    @Autowired
    private CatalogRepo catalogRepo;

    @Autowired
    private PeriodicalRepo periodicalRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PriceRepo priceRepo;


   /* public List<CatalogsDTO> getList(String path){
        if (mainList == null){createList();}
        List<CatalogsDTO> result = new ArrayList<>();
        if (mainList.isEmpty()) return result;
        String[] folders = path.split("-");
        if (folders.length > 0){
            *//*Create first dto*//*
            List<Catalog> list = mainList.stream().filter(e -> e.getParent().equals(e)).collect(Collectors.toList());
            CatalogsDTO catalogsDTO = new CatalogsDTO();
            catalogsDTO.setCatalogs(list);
            if (folders[0].equals("new") || validId(folders[0], mainList)){
                catalogsDTO.setId(list.get(0).getId());//change first catalog

            } else {catalogsDTO.setId(Long.parseLong(folders[0]));}
            result.add(catalogsDTO);

            *//*Next catalogs*//*
            for (int i = 1; i < folders.length; i++) {
                CatalogsDTO nextCatalogsDTO = new CatalogsDTO();
                List<Catalog> children = findCatalog(result.get(result.size() - 1).getId()).getChildren()
                        .stream().collect(Collectors.toList());
                if (!children.isEmpty()){nextCatalogsDTO.setCatalogs(children);
                } else {break;}
                if (folders[i].equals("new") || validId(folders[i], children)){
                    nextCatalogsDTO.setId(list.get(0).getId());//change first catalog
                } else {nextCatalogsDTO.setId(Long.parseLong(folders[0]));}
                result.add(nextCatalogsDTO);
            }
        }
        return result;
    }*/

   /* private Catalog findCatalog(long id) {
        return mainList.stream().filter(e -> e.getId() == id).findAny().get();
    }

    private boolean validId(String id, List<Catalog> list) {
        long findId = Long.parseLong(id);
        return list.stream().filter(e -> e.getId() == findId).count() == 1;
    }

    public void writeCatalog(Integer number, Catalog oldCatalog, HttpServletRequest req) {
        if (number > 0){
            long parentId = 0;

            *//*Find last catalog*//*
            for (int i = number; i > 0; i--) {
                if (req.getParameter("catalog"+i) != null){
                    parentId = Integer.parseInt(req.getParameter("catalog"+i));
                    break;
                }
            }
            Catalog catalog;
            if (oldCatalog.getId() == null){
                catalog = oldCatalog;

            } else {
                catalog = catalogRepo.findById(oldCatalog.getId()).get();
                catalog.setNameUa(oldCatalog.getNameUa());
                catalog.setNameEn(oldCatalog.getNameEn());
            }
            catalog.setParent(new ArrayList<Catalog>(Collections.singleton(catalogRepo.findById(parentId).get())));

        }
    }*/

    public boolean addCatalog(CatalogDTO catalogDTO) {
        /*Checked catalog in database*/
        if (catalogRepo.existsByNameUa(catalogDTO.getNameUa()) ||
        catalogRepo.existsByNameEn(catalogDTO.getNameEn())){return false;}

        Catalog catalog = new Catalog();
        catalog.setNameUa(catalogDTO.getNameUa());
        catalog.setNameEn(catalogDTO.getNameEn());
        catalogRepo.save(catalog);
        return true;
    }

    public boolean editCatalog(Catalog catalog, CatalogDTO catalogDTO) {
        if (catalog != null){
            catalog.setNameUa(catalogDTO.getNameUa());
            catalog.setNameEn(catalogDTO.getNameEn());
            catalogRepo.save(catalog);
            return true;
        }
        return false;
    }

    public boolean deleteCatalog(Catalog catalog) {
        if (catalog != null){
            catalogRepo.delete(catalog);
            return true;
        }
        return false;
    }

    public boolean addPeriodical(PeriodicalDTO periodicalDTO, MultipartFile file) {
        Periodical periodical = new Periodical();
        if (file != null){
            String dir =  System.getProperty("user.dir") + File.separator +"src/main/resources/static/img";
            File uploadDir = new File(dir);
            if(uploadDir.exists()){uploadDir.mkdir();}
            String fileName = uploadDir + File.separator + file.getOriginalFilename();
            try {
                file.transferTo(new File(fileName) );
                periodical.setPathImage("img/"+file.getOriginalFilename());
            } catch (IOException e) {
                log.error(e.getMessage());
                return false;
            }
        } else {periodical.setPathImage("img/cards/noImage.jpg");}
        String refactorName = periodicalDTO.getName().trim().toLowerCase().replaceAll("\\s","");
        periodical.setFindName(refactorName);
        periodical.setName(periodicalDTO.getName());
        periodical.setInfo(periodicalDTO.getInfo());
        Price price = new Price(LocalDate.now(), LocalDate.now(), periodicalDTO.getPrice());
        periodical.setPrices(Collections.singleton(price));
        periodical.setCatalogs(Collections.singleton(catalogRepo.findById(periodicalDTO.getCatalog()).get()));
        periodicalRepo.save(periodical);
        price.setPeriodical(periodical);
        priceRepo.save(price);
        return true;
    }

    public boolean editPeriodical(PeriodicalDTO periodicalDTO, MultipartFile file, Periodical periodical) {
        if (periodical == null){return false;}
        if (file != null && !file.getOriginalFilename().isEmpty() && periodical.getPathImage().equals(
                "img/" + file.getOriginalFilename())){
            String dir =  System.getProperty("user.dir") + File.separator +"src/main/resources/static";
            Path deletePath = Paths.get( dir + File.separator + periodical.getPathImage());
            if (Files.exists(deletePath)) {
                try {
                    Files.delete(deletePath);
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
            File uploadDir = new File(dir + File.separator + "img");
            if(uploadDir.exists()){uploadDir.mkdir();}
            String fileName = uploadDir + "/" + file.getOriginalFilename();
            try {
                file.transferTo(new File(fileName) );
                periodical.setPathImage("img/cards/"+file.getOriginalFilename());
            } catch (IOException e) {
                log.error(e.getMessage());
                return false;
            }
        }
        String refactorName = periodicalDTO.getName().trim().toLowerCase().replaceAll("\\s","");
        periodical.setFindName(refactorName);
        periodical.setName(periodicalDTO.getName());
        periodical.setInfo(periodicalDTO.getInfo());
        periodical.getCatalogs().retainAll(Collections.singleton(catalogRepo.findById(periodicalDTO.getCatalog()).get()));
//        periodical.setCatalogs();
        Price price;
        if (periodical.getPrices().isEmpty()){
            price = new Price();
            price.setStartData(LocalDate.now());
            price.setEndData(LocalDate.now());
        } else {price = (Price) periodical.getPrices().toArray()[0];}
        price.setPrice(periodicalDTO.getPrice());
        periodical.getPrices().retainAll(Collections.singleton(price));
//        periodical.setPrices();
        periodicalRepo.save(periodical);
        price.setPeriodical(periodical);
        priceRepo.save(price);
        return true;
    }

    public boolean deletePeriodical(Periodical periodical) {
        if (periodical != null){
            periodicalRepo.delete(periodical);
            return true;
        }
        return false;
    }

    public Periodical getPeriodical(long id) {
        return periodicalRepo.findById(id).get();
    }
}