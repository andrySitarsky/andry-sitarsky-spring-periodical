package com.ua.epam.project.services;

import com.ua.epam.project.dto.SaleDTO;
import com.ua.epam.project.entities.Periodical;
import com.ua.epam.project.entities.Sale;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.entities.UserInfo;
import com.ua.epam.project.repos.SaleRepo;
import com.ua.epam.project.repos.UserInfoRepo;
import com.ua.epam.project.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private SaleRepo saleRepo;
    @Autowired
    private UserInfoRepo userInfoRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public boolean addMoney(User user, String money) {
        if (user != null){
            user.setBalance(user.getBalance().add(BigDecimal.valueOf(Double.parseDouble(money))));
            userRepo.save(user);
            return true;
        }
        return false;
    }

    public boolean subscribe(SaleDTO saleDTO, Periodical periodical, User user) {
        BigDecimal amount = getAmount(saleDTO, (double) periodical.getPrices().toArray()[0]);
        BigDecimal subtract = user.getBalance().subtract(amount);
        if (subtract.doubleValue() > 0){
        Sale sale = new Sale();
        sale.setDate(LocalDate.now());
        sale.setUser(user);
        sale.setDateStart(saleDTO.getFromData());
        sale.setDateEnd(saleDTO.getToData());
        sale.setNumber(saleDTO.getNumber());
        sale.setPeriodical(periodical);
        sale.setAmount(amount);
        saleRepo.save(sale);
        user.setBalance(subtract);
        userRepo.save(user);
        return true;}
        return false;
    }

    private BigDecimal getAmount(SaleDTO saleDTO, double price) {
        double amount = Period.between(saleDTO.getFromData(), saleDTO.getToData()).getDays() * saleDTO.getNumber() * price;
        return BigDecimal.valueOf(amount);
    }

    public UserInfo getUserInfo(User user) {
        return userInfoRepo.findById(user.getId()).orElse(new UserInfo());
    }
}
