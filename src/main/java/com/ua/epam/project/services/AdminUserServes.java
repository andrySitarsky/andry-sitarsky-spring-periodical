package com.ua.epam.project.services;

import com.ua.epam.project.entities.Sale;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminUserServes {
    @Autowired
    private UserRepo userRepo;

    public boolean blockedUser(User user) {
        if (user != null){
            user.setActive(!user.getActive());
            userRepo.save(user);
            return true;
        }
        return false;
    }

    public void changeRole(User user) {
        if (user != null){
            user.getRole().retainAll(Collections.singleton(user.getRole().contains(Role.USER) ? Role.ADMIN : Role.USER));
            userRepo.save(user);
        }
    }

    public List<User> getUsers() {
        return userRepo.findAll();
    }

    public List<String> getRoles(List<User> users) {
        return users.stream().map(e -> ((Role) e.getRole().toArray()[0]).name()).collect(Collectors.toList());
    }

    public List<Double> getSums(List<User> users) {
        return users.stream().map(e -> e.getSales().stream().map(el -> el.getAmount().doubleValue())
                .collect(Collectors.summingDouble(Double::doubleValue))).collect(Collectors.toList());
    }
}
