package com.ua.epam.project.services;

import com.ua.epam.project.entities.Catalog;
import com.ua.epam.project.entities.Periodical;
import com.ua.epam.project.entities.Price;
import com.ua.epam.project.repos.CatalogRepo;
import com.ua.epam.project.repos.PeriodicalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class HomeService {
    @Autowired
    private CatalogRepo catalogRepo;
    @Autowired
    private PeriodicalRepo periodicalRepo;

    public Specification<Periodical> getDateNow(){
        return  new Specification<Periodical>() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                LocalDate data = LocalDate.now();
                Join<Periodical, Price> prices = root.join("prices");
                predicates.add(criteriaBuilder.lessThanOrEqualTo(prices.get("startData"),data));
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(prices.get("endData"),data));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }

    public Specification<Periodical> getPeriodicalInCatalog(long id){
        return (root, query, criteriaBuilder) -> {
            Join<Periodical, Catalog> catalogs = root.join("catalogs");
            return id == -1 ? criteriaBuilder.isNull(catalogs.get("id")) :
                    criteriaBuilder.equal(catalogs.get("id"), id);
        };
    }

    public Specification<Periodical> sortPeriodicalNameByPrice(String name, boolean isASC){
        return (root, query, criteriaBuilder) -> {
            Join<Periodical, Price> priceJoin = root.join("prices");
            if (isASC){query.orderBy(criteriaBuilder.asc(priceJoin.get("price")));
            } else {query.orderBy(criteriaBuilder.desc(priceJoin.get("price")));}
            return criteriaBuilder.like(root.<String>get("findName"), "%" + name + "%");
        };
    }

    public Specification<Periodical> sortPeriodicalTopicByPrice(long id, boolean isASC){
        return (root, query, criteriaBuilder) -> {
            Join<Periodical, Price> priceJoin = root.join("prices");
            Join<Periodical, Catalog> catalogJoin = root.join("catalogs");
            if (isASC){query.orderBy(criteriaBuilder.asc(priceJoin.get("price")));
            } else {query.orderBy(criteriaBuilder.desc(priceJoin.get("price")));}
            return criteriaBuilder.equal(catalogJoin.<Long>get("id"), id);
        };
    }

    public Periodical getCard(long id) {
       return periodicalRepo.findById(id).orElse(new Periodical());
    }


    public Page<Periodical> findName(String name, Pageable pageable) {
        if (name == null || name.isEmpty()){return periodicalRepo.findAll(pageable);
        } else {
        String refactorName = name.trim().toLowerCase().replaceAll("\\s","");
        return periodicalRepo.findByFindName(refactorName, pageable);}
    }

    public Iterable<Catalog> getList() {
        return catalogRepo.findAll();
    }

    public Pageable getPageable(Integer page, Integer size, String sort, HttpSession session) {
        Pageable pageable;
        boolean isName = false;
        if (!session.isNew() && session.getAttribute("page") != null){
            Page<Periodical> oldPage = (Page<Periodical>) session.getAttribute("page");
            Sort newSort;
            if (sort != null){
                String[] params = sort.split(",");
                newSort = Sort.by(params[0]);
                if(params[0].equals("name")){
                    if (params[1].equals("ASC")){newSort = newSort.ascending();
                    } else {newSort = newSort.descending();}
                    isName = true;
                }

            } else {newSort = oldPage.getSort();}
            if (isName){
            pageable = PageRequest.of(page != null ? page : oldPage.getNumber() ,
                    size != null ? size : oldPage.getSize(), newSort);
            } else {pageable = PageRequest.of(page != null ? page : oldPage.getNumber() ,
                    size != null ? size : oldPage.getSize());}

        } else {pageable = PageRequest.of(0, 6);}
        return pageable;
    }

    public Page<Periodical> findTopic(long id, Pageable pageable) {
        return periodicalRepo.findAll(getPeriodicalInCatalog(id), pageable);
    }

    public Page<Periodical> pageablePrice(Integer page, Integer size, String sort, HttpSession session) {
        Pageable pageable = getPageable(page, size, sort, session);
        String[] params = sort.split(",");
        boolean isASC = params[1].equalsIgnoreCase("ASC");
        Specification<Periodical> specification;
        String type = (String) session.getAttribute("typeFind");
        if (type.equals("name")){
            String findName = (String) session.getAttribute("findName");
            if (findName.isEmpty()){specification = sortPeriodicalNameByPrice("", isASC);
            } else {specification = sortPeriodicalNameByPrice(findName, isASC);}
        } else {
            long id = (long) session.getAttribute("currencyId");
            specification = sortPeriodicalTopicByPrice(id, isASC);
        }

        return periodicalRepo.findAll(specification,pageable);
    }
}
