package com.ua.epam.project.services;

import com.ua.epam.project.dto.RegistrationDTO;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.entities.UserInfo;
import com.ua.epam.project.enums.Gender;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.repos.UserInfoRepo;
import com.ua.epam.project.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;

@Service
public class ObtainUserServes {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserInfoRepo userInfoRepo;

    private PasswordEncoder encoder = new BCryptPasswordEncoder(8);

    public void registration(User user, RegistrationDTO registrationDTO) {
        User newUser;
        UserInfo userInfo;
        if (user == null){
            newUser = new User();
            newUser.setBalance(BigDecimal.ZERO);
            newUser.setActive(true);
            newUser.setRole(Collections.singleton(Role.USER));
            userInfo = new UserInfo();
            } else {newUser = user;
        userInfo = userInfoRepo.findById(user.getId()).orElse(new UserInfo());}


        userInfo.setCity(registrationDTO.getCity());
        userInfo.setDate(registrationDTO.getDate());
        userInfo.setFirstName(registrationDTO.getFirstName());
        userInfo.setLastName(registrationDTO.getLastName());
        userInfo.setRole(Collections.singleton(Gender.valueOf(registrationDTO.getGender().toUpperCase())));
        userInfoRepo.save(userInfo);

        userInfo.setUser(newUser);
        newUser.setNickName(registrationDTO.getNickName());
        newUser.setEmail(registrationDTO.getEmail());
        newUser.setLogin(registrationDTO.getLogin());
        newUser.setPassword(encoder.encode(registrationDTO.getPassword()));
        userRepo.save(newUser);



    }
}
