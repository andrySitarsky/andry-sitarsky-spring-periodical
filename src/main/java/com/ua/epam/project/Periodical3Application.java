package com.ua.epam.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Periodical3Application {

	public static void main(String[] args) {
		SpringApplication.run(Periodical3Application.class, args);
	}

}
