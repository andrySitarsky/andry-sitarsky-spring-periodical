package com.ua.epam.project.tld;

import com.ua.epam.project.entities.Catalog;

import java.util.List;

public interface SettingMenu {
    List<Catalog> getList();
    Catalog getCatalog();
    boolean isEdit();
    String getLocale();
    String menuAll();
}
