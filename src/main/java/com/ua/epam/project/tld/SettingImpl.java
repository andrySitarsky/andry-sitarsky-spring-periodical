package com.ua.epam.project.tld;

import com.ua.epam.project.entities.Catalog;
import lombok.Builder;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Builder
public class SettingImpl implements SettingMenu, Serializable {
    private List<Catalog> list;
    private Catalog catalog;
    private boolean edit;
    private String locale;
    private String all;

    @Override
    public List<Catalog> getList() {
        return list;
    }

    @Override
    public Catalog getCatalog() {
        return catalog;
    }

    @Override
    public boolean isEdit() {
        return edit;
    }

    @Override
    public String getLocale() {
        return locale;
    }

    @Override
    public String menuAll() {
        return all;
    }
}
