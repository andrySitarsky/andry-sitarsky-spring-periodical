package com.ua.epam.project.tld;

import com.ua.epam.project.entities.Catalog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

public class AccordionMenuTld {
    private SettingMenu settingMenu;
    private Stack<Catalog> parents;


    public String printMenu(SettingMenu settingMenu){
        this.settingMenu = settingMenu;
        parents = getParents(settingMenu.getCatalog());
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            factory.setNamespaceAware(true);
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document document = builder.newDocument();
        buildDocument(document);
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        try {
            transformer.transform(source,result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return subDoc(writer.toString());
    }

    /**Delete <?xml version="1.0" encoding="UTF-8" standalone="no"?> */
    private String subDoc(String doc) {
        int position = doc.indexOf(">");
        return doc.substring(position+1);
    }
    /**Form list of parents*/
    private Stack<Catalog> getParents(Catalog catalog){
        Stack<Catalog> parents = new Stack<>();
        parents.push(catalog);
        while (!parents.peek().getParent().isEmpty()){
           parents.push(parents.peek().getParent().get(0));
        }
        return parents;
    }

    private void buildDocument(Document document) {
        Element root = document.createElement("div");
        root.setAttribute("class", "accordion");
        root.setAttribute("id", "item0");
        buildElement(document,root, getFirstSet());
        document.appendChild(root);
    }

    private Set<Catalog> getFirstSet(){
        return settingMenu.getList().stream().filter(e -> e.getParent().isEmpty())
                .collect(Collectors.toSet());
    }


    private void buildElement(Document document, Element root, Set<Catalog> catalogs) {
        if (catalogs.isEmpty()) return;
        catalogs.forEach((Catalog e) -> {
            if (e.getChildren().size() > 1){
                buildFolder(document, e, root);
            }else {
                buildSimpleElement(document, e, root);
            }
        });
    }


    private void buildFolder(Document document, Catalog catalog, Element root) {
        Element divRoot = document.createElement("div");
        divRoot.setAttribute("class", "accordion-item ps-0");

        Element h2 = document.createElement("h2");
        h2.setAttribute("class", "accordion-header");
        h2.setAttribute("id","heading"+catalog.getId());
        Element button = document.createElement("button");
        button.setAttribute("class","accordion-button collapsed");
        button.setAttribute("type","button");
        button.setAttribute("data-bs-toggle","collapse");
        button.setAttribute("data-bs-target","#item"+catalog.getId());
        button.setAttribute("aria-expanded", String.valueOf(parents.contains(catalog)));
        button.setAttribute("aria-controls","item"+catalog.getId());
        if (settingMenu.isEdit()){
            buildSpanButton(document, catalog, button);}
        Element divButton = document.createElement("div");
        divButton.setAttribute("class","col-8");
        if (settingMenu.getLocale().equalsIgnoreCase("ua")){
            divButton.setTextContent(catalog.getNameUa());
        } else {divButton.setTextContent(catalog.getNameEn());}
        button.appendChild(divButton);
        if (settingMenu.isEdit()){
            Element span2 = document.createElement("span");
            span2.setAttribute("class","form-check col-1");
            Element inputCheck = document.createElement("input");
            inputCheck.setAttribute("class","form-check-input");
            inputCheck.setAttribute("type","checkbox");
            inputCheck.setAttribute("name","option"+catalog.getId());
            span2.appendChild(inputCheck);
            button.appendChild(span2);}
        h2.appendChild(button);
        divRoot.appendChild(h2);

        Element divNext = document.createElement("div");
        divNext.setAttribute("id","item"+catalog.getId());
        divNext.setAttribute("class","accordion-collapse collapse"+
                (parents.contains(catalog) ? "show" : ""));
        divNext.setAttribute("aria-labelledby","heading"+catalog.getId());

        /*Form link to parent*/
        divNext.setAttribute("data-bs-parent","#item"+
                (catalog.getParent().isEmpty() ? "0" : catalog.getParent().get(0).getId()));
        Element divChild = document.createElement("div");
        divChild.setAttribute("class","accordion-body");
        Element ul = document.createElement("ul");
        ul.setAttribute("class","btn-toggle-nav list-unstyled fw-normal ps-0");

        createLi(document,catalog,ul,true);

        buildElement(document,ul,catalog.getChildren());
        divChild.appendChild(ul);
        divNext.appendChild(divChild);
        divRoot.appendChild(divNext);
        root.appendChild(divRoot);
    }

    private void buildSpanButton(Document document, Catalog catalog, Element button) {
        Element divSpan = document.createElement("div");
        divSpan.setAttribute("class", "col-2");
        Element span1 = document.createElement("span");
        span1.setAttribute("class", "btn btn-primary");
        span1.setAttribute("data-bs-toggle", "modal");
        span1.setAttribute("data-bs-target", "#myModal");
        span1.setAttribute("onclick", "getCatalogs('edit','" + catalog.getId() + "')");
        span1.setTextContent("+");
        divSpan.appendChild(span1);
        button.appendChild(divSpan);
    }

    /**Create div. Put on span1, span2, li  in it. Put on div in root*/
    private void buildSimpleElement(Document document, Catalog catalog, Element root) {
        Element child = document.createElement("div");
        child.setAttribute("class", "row");
        if (settingMenu.isEdit()){
            buildSpanButton(document, catalog, child);
        }
        createLi(document, catalog, child, false);
        if (settingMenu.isEdit()){
            Element span2 = document.createElement("span");
            span2.setAttribute("class", "form-check col-2");
            Element input = document.createElement("input");
            input.setAttribute("class","form-check-input");
            input.setAttribute("type","checkbox");
            input.setAttribute("name", "option"+ catalog.getId());
            span2.appendChild(input);
            child.appendChild(span2);
        }
        root.appendChild(child);
    }

    private void createLi(Document document, Catalog catalog, Element child, boolean all) {
        Element li = document.createElement("li");
        li.setAttribute("class","col");
        Element a = document.createElement("a");
        a.setAttribute("href","periodical?command=home.FindCod&id_topic="+ catalog.getId());
        StringBuilder param = new StringBuilder().append("d-inline-flex text-decoration-none rounded");
        if (catalog.equals(settingMenu.getCatalog())){param.append("link-red ");
        } else {param.append("link-dark");}
        a.setAttribute("class", param.toString());
        a.setTextContent((all ? settingMenu.menuAll() : "") + " " +
                (settingMenu.getLocale().equalsIgnoreCase("ua") ? catalog.getNameUa()
                        : catalog.getNameEn()));
        li.appendChild(a);
        child.appendChild(li);
    }
}
