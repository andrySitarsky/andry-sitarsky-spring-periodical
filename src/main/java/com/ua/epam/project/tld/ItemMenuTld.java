package com.ua.epam.project.tld;

import com.ua.epam.project.entities.Catalog;

import java.util.Stack;

public class ItemMenuTld {
    /**
     * Form string: Parent->SubParent->Child
     */
    public String printItem(SettingMenu settingMenu) {
        Stack<Catalog> parents = getParents(settingMenu.getCatalog());
        StringBuffer result = new StringBuffer();
        while (!parents.isEmpty()) {
            Catalog catalog = parents.pop();
            result.append(settingMenu.getLocale().equalsIgnoreCase("ua")
                    ? catalog.getNameUa() : catalog.getNameEn()).append(" -> ");
        }
        /*Delete end symbol -> */
        result.replace(result.length() - 4, result.length() - 1, "");
        return result.toString();
    }

    /**
     * Form list of parents
     */
    private Stack<Catalog> getParents(Catalog catalog) {
        Stack<Catalog> parents = new Stack<>();
        parents.push(catalog);
        while (!parents.peek().getParent().isEmpty()) {
            parents.push(parents.peek().getParent().get(0));
        }
        return parents;
    }
}
