package com.ua.epam.project.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PeriodicalDTO {
    private Long catalog;
    private BigDecimal price;
    private String name;
    private String info;
}
