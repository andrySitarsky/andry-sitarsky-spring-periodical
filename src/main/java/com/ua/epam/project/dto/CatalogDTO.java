package com.ua.epam.project.dto;

import lombok.Data;

@Data
public class CatalogDTO {
    private Long id;
    private String nameUa;
    private String nameEn;
}
