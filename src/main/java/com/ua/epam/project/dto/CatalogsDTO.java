package com.ua.epam.project.dto;

import com.ua.epam.project.entities.Catalog;
import lombok.Data;

import java.util.List;

@Data
public class CatalogsDTO {
    private long id;
    private List<Catalog> catalogs;
}
