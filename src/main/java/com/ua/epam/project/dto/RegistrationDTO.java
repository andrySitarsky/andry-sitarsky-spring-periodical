package com.ua.epam.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class RegistrationDTO {
    private String firstName;
    private String lastName;
    private String city;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String nickName;
    private String login;
    private String password;
    private String email;
    private String gender;
}
