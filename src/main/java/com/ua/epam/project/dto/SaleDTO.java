package com.ua.epam.project.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SaleDTO {
    private LocalDate fromData;
    private LocalDate toData;
    private int number;
}
