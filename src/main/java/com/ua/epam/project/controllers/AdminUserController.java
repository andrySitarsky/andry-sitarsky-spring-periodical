package com.ua.epam.project.controllers;

import com.ua.epam.project.entities.User;
import com.ua.epam.project.services.AdminUserServes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class AdminUserController {
    @Autowired
    private AdminUserServes serves;

    @GetMapping("/admin/showUsers")
    public String showUsers(Model model){
        List<User> users = serves.getUsers();
        model.addAttribute("users", users);
        model.addAttribute("roles", serves.getRoles(users));
        model.addAttribute("sums", serves.getSums(users));
        return "admin/admin";
    }

    @PostMapping("/admin/active/{id}")
    public String blockedUser(@PathVariable("id") User user){
        serves.blockedUser(user);
        return "redirect: /admin/showUsers";
    }
    @PostMapping("/admin/changeRole/{id}")
    public String changeRole(@PathVariable("id") User user){
        serves.changeRole(user);
        return "redirect: /admin/showUsers";
    }
}
