package com.ua.epam.project.controllers;

import com.ua.epam.project.entities.Catalog;
import com.ua.epam.project.entities.Periodical;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.repos.CatalogRepo;
import com.ua.epam.project.repos.PeriodicalRepo;
import com.ua.epam.project.services.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class MainController {
    @Autowired
    private CatalogRepo repo;

    @Autowired
    private PeriodicalRepo periodicalRepo;

    @Autowired
    private HomeService homeService;


    private PasswordEncoder encoder = new BCryptPasswordEncoder(8);


    @GetMapping("/")
    public String index(Model model, HttpSession session){
        return findName("", model, PageRequest.of(0, 6), session);}


    @GetMapping("/guest/pageable")
    public String pageable(@RequestParam (required=false , name = "page") Integer page,
                               @RequestParam(required=false, name = "size") Integer size,
                               @RequestParam(required=false, name = "sort") String sort,
                               Model model,
                               HttpSession session){
       if (sort != null){
           String[] param = sort.split(",");
           if (param[0].equals("price")){
               return pageablePrice(page, size, sort, model, session);
           }
       }
       return pageableName(page, size, sort, model, session);
    }

    @GetMapping("/guest/pageableName")
    public String pageableName(@RequestParam (required=false, name = "page") Integer page,
                           @RequestParam(required=false, name = "size") Integer size,
                           @RequestParam(required=false, name = "sort") String sort,
                           Model model,
                           HttpSession session){
        Pageable  pageable = homeService.getPageable(page, size, sort, session);
        if (((String)session.getAttribute("typeFind")).equals("topic")){
            return findTopic((Long) session.getAttribute("idCard"), model, pageable, session);
        }

        return findName((String) session.getAttribute("findName"), model,  pageable, session);
    }

    @GetMapping("/guest/pageablePrice")
    public String pageablePrice(@RequestParam (required=false, name = "page") Integer page,
                           @RequestParam(required=false, name = "size") Integer size,
                           @RequestParam(required=false, name = "sort") String sort,
                           Model model,
                           HttpSession session){
        Page<Periodical> newPage = homeService.pageablePrice(page,size, sort, session);
        settingPage(model, session, newPage);
        if (session.getAttribute("findName") != null){
        model.addAttribute("findName", (String)session.getAttribute("findName"));
        } else {model.addAttribute("currencyId", session.getAttribute("currencyId"));}
        model.addAttribute("param", "/guest/findName");
        model.addAttribute("editPage", true);
        model.addAttribute("typePage","home");
        return "home/index";
    }

    @GetMapping("/backHome")
    public String back(Model model, HttpSession session){
        Page<Periodical> page = (Page<Periodical>) session.getAttribute("page");
        if (page == null) return "redirect:/";
        settingPage(model, session, page);
        return "home/index";
    }

    @GetMapping("/guest/card/{id}")
    public  String card(@PathVariable("id") long id, Model model){
        model.addAttribute("card", homeService.getCard(id));
        return "card/card-page";
    }


    @GetMapping("/guest/findName")
    public String findName(@RequestParam("name") String name, Model model,
                           @PageableDefault(sort = { "id" }, size = 6,
                                   direction = Sort.Direction.DESC) Pageable pageable, HttpSession session){
        Page<Periodical> newPage = homeService.findName(name, pageable);
        settingPage(model, session, newPage);
        session.setAttribute("typeFind", "name");
        model.addAttribute("findName", name);
        session.setAttribute("findName", name);
        return "home/index";
    }

    private void settingPage(Model model, HttpSession session, Page<Periodical> newPage) {
        if (newPage.getTotalElements() > newPage.getSize()){
            model.addAttribute("totalPages", new int[newPage.getTotalPages()]);
        }
        Iterable<Catalog> list = homeService.getList();
        model.addAttribute("listCatalogs", list);
        model.addAttribute("page", newPage);
        session.setAttribute("listCatalog", list);
        session.setAttribute("page", newPage);
        model.addAttribute("editPage", true);
        model.addAttribute("typePage","home");
    }

    @GetMapping("/guest/findTopic/{id}")
    public String findTopic(@PathVariable("id") long id, Model model,
                            @PageableDefault(sort = { "id" }, size = 6,
                                    direction = Sort.Direction.DESC) Pageable pageable, HttpSession session){
        Page<Periodical> newPage = homeService.findTopic(id, pageable);
        settingPage(model, session, newPage);
        session.setAttribute("typeFind", "topic");
        model.addAttribute("currencyId", id);
        session.setAttribute("currencyId", id);
        model.addAttribute("param", "/guest/findTopic/"+id);
        return "home/index";
    }



    @GetMapping("/hello")
    public String periodical(Model model, @PageableDefault(sort = { "id" }, size = 6,
            direction = Sort.Direction.DESC) Pageable pageable){

        Page<Periodical> periodicals = periodicalRepo.findAll(pageable);
        periodicals.getSort().toString();
        periodicals.getTotalElements();

        model.addAttribute("catalogs", periodicals);
        model.addAttribute("title", "Andrey");
        return "hello";
    }


    @PostMapping("/")
    public String editPage(Model model, @PageableDefault(sort = { "id" }, size = 6,
            direction = Sort.Direction.DESC) Pageable pageable){
        model.addAttribute("page", periodicalRepo.findAll(pageable));
        model.addAttribute("editPage", true);
        return "home/index";}
    }