package com.ua.epam.project.controllers;

import com.ua.epam.project.dto.PeriodicalDTO;
import com.ua.epam.project.dto.SaleDTO;
import com.ua.epam.project.entities.Periodical;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.repos.UserRepo;
import com.ua.epam.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private MainController mainController;

    @GetMapping("/user/showAccount")
    public String showAccount(@AuthenticationPrincipal User user, Model model, HttpSession session){
        model.addAttribute("user", user);
        return mainController.index(model, session);
    }

    @PostMapping("/user/addMoney")
    public String addMoney(@AuthenticationPrincipal User user, @RequestParam("addMoney") String money, Model model){
        userService.addMoney(user, money);
        return "redirect:/user/showAccount";
    }
    @PostMapping("/user/subscribe")
    public String subscribe(SaleDTO saleDTO, @RequestParam("idPeriodical")Periodical periodical,
                            @AuthenticationPrincipal User user){
        userService.subscribe(saleDTO, periodical, user);
        return "redirect:/";
    }
    @GetMapping("/user/showSubscribe")
    public String showSubscribe(@AuthenticationPrincipal User user, Model model, HttpSession session){
        model.addAttribute("sales", user.getSales().iterator());
        return "subscribe/subscribe";
    }
    @GetMapping("/user/showInfo")
    public String showInfo(@AuthenticationPrincipal User user, Model model){
        model.addAttribute("user", user);
        model.addAttribute("userInfo", userService.getUserInfo(user));
        return "registration/registration";
    }

}
