package com.ua.epam.project.controllers;

import com.ua.epam.project.dto.CatalogDTO;
import com.ua.epam.project.dto.CatalogsDTO;
import com.ua.epam.project.dto.PeriodicalDTO;
import com.ua.epam.project.entities.Catalog;
import com.ua.epam.project.entities.Periodical;
import com.ua.epam.project.entities.Price;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.services.AdminServes;
import com.ua.epam.project.services.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Controller
public class AdminController {
    @Autowired
    private AdminServes adminServes;
    @Autowired
    private HomeService homeService;

    @GetMapping("/admin/newCard")
    public String newCard(Model model){
        model.addAttribute("editPage", true);
        model.addAttribute("listCatalogs", homeService.getList());
        return "card/card-page";
    }

    @GetMapping("/admin/editCard/{id}")
    public String editCard(@PathVariable("id") Periodical periodical, Model model){
        Catalog catalog;
        if (!periodical.getCatalogs().isEmpty()){
            catalog = (Catalog) periodical.getCatalogs().toArray()[0];
            model.addAttribute("idCatalog", catalog.getId());
        }

        model.addAttribute("price", (!periodical.getPrices().isEmpty()
                ?((Price) periodical.getPrices().toArray()[0]).getPrice() : 0));
        model.addAttribute("card", periodical);
        model.addAttribute("editPage", true);
        model.addAttribute("listCatalogs", homeService.getList());
        return "card/card-page";
    }

    @PostMapping("/admin/addCatalog")
    public String addCatalog(CatalogDTO catalogDTO){
        adminServes.addCatalog(catalogDTO);
        return "redirect:/";
    }

    @PostMapping("/admin/editCatalog/{id}")
    public String editCatalog(@PathVariable("id") Catalog catalog, CatalogDTO catalogDTO){
        adminServes.editCatalog(catalog, catalogDTO);
        return "redirect:/";
    }

    @GetMapping("/admin/deleteCatalog/{id}")
    public String deleteCatalog(@PathVariable("id") Catalog catalog){
        adminServes.deleteCatalog(catalog);
        return "redirect:/";
    }

    @PostMapping("/admin/addPeriodical")
    public String addPeriodical(PeriodicalDTO periodicalDTO, @RequestParam("file") MultipartFile file, HttpServletRequest req){
        adminServes.addPeriodical(periodicalDTO, file);
        return "redirect:/";
    }

    @PostMapping("/admin/editPeriodical/{id}")
    public String editPeriodical(PeriodicalDTO periodicalDTO, @PathVariable("id") Periodical periodical,
                                 @RequestParam("file") MultipartFile file){
        adminServes.editPeriodical(periodicalDTO, file, periodical);
        return "redirect:/";
    }

    @GetMapping("/admin/deletePeriodical/{id}")
    public String deletePeriodical(@PathVariable("id") Periodical periodical){
        adminServes.deletePeriodical(periodical);
        return "redirect:/";
    }

}
