package com.ua.epam.project.controllers;

import com.ua.epam.project.dto.RegistrationDTO;
import com.ua.epam.project.entities.User;
import com.ua.epam.project.services.ObtainUserServes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ObtainUserController {
    @Autowired
    private ObtainUserServes serves;

    @GetMapping("/registration")
    public  String registration(){
        return "registration/registration";
    }

    @PostMapping("/writeRegistration")
    public String writeRegistration(@AuthenticationPrincipal User user ,
                                    RegistrationDTO registrationDTO){
        serves.registration(user, registrationDTO);
        return "redirect:/";
    }
}
