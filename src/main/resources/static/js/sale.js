    $(document).ready(function(){
        $('#numberSale').on('change', function () {
         var date1 = new Date($('#date_start_sale').val());
         var date2 = new Date($('#date_end_sale').val());
         var daysLag = Math.ceil(Math.abs(date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
         var amount = Number($(this).val()) * Number($('#priceSale').val()) * daysLag;
         $('#amountSale').text(amount);
    });
